$(document).ready(function () {

    $(".modal input[name=encargado]").val(encargado);

    listar();
    proyectos();
    //listarContacto();

    $(".filtros select[name=estado]").bind("change", function () {
        var est = $(this).val();
        listar(est);
    });

    

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modalagregar").modal("hide");
                $(".modal input[name=encargado]").val(encargado);
                $("#modalagregar select[name=rol]").val("");
                proyectos();
                listar();
            }else{
                alert(res.res);
                $("#modalagregar").modal("hide");
                $(".modal input[name=encargado]").val(encargado);
                $("#modalagregar select[name=rol]").val("");
                proyectos();
            }
        }
    }

    $("#modalagregar form").ajaxForm(options);

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modaleditar").modal("hide");
                listar(res.estado);
                $(".filtros select[name=estado]").val(res.estado);
            }
        }
    }
    $("#modaleditar form").ajaxForm(options);

    /*html.find(".btn-editar").click(function () {
        alert("HOLA");
        $("#modaleditar").modal("show");
    });*/

});


function proyectos(){
    new Request("proyecto/listado/",{
        estado: 1,
        usr_id: idUsuario,
        usr_tipo: tipoUsuario
    },function(res){
        console.log(res);
        $.each(res,function(k,v){
            var option = '<option value="'+v.pry_id+'">'+v.pry_descripcion+'</option>'
            $("select[name=proyecto]").append(option);
            // $("#modalagregar #cat_add select[name=categoria]").append(option);
        });

         //listar();
    });
}



function listar(est) {
    console.log('log');
    $("#designar .lista").empty();

    var pry_id = "";

    new Request("roles/listar/", {
        usr_id : encargado,
        pry_id : pry_id
    }, function (res) {
        $("#designar .lista").empty();
        $.each(res, function (k, v) {
            var it = new ItemProyectos(v);
            $("#designar .lista").append(it);
        })

    });


}


var ItemProyectos = function (data) {

    var id_proyecto = data.id_proyecto;
    var proyecto = data.proyecto;
    var tipoproyecto = data.tipo_proyecto;

    var rol = data.roles;

    var idusuario = data.idusuario;

    if(rol == "1"){
        roldescripcion = "General";
    }
    if(rol == "2"){
        roldescripcion = "Contactos";   
    }
    if(rol == "3"){
        roldescripcion = "Referidos";
    }
    if(rol == "4"){
        roldescripcion = "Whatsapp";   
    }

    //var id_proyecto_web = data.id_proyecto_web;

    var html = $('<tr width="100%">' +
        '<td style="vertical-align:middle;">' + id_proyecto + '</td>' +
        '<td style="vertical-align:middle;">' + proyecto + '</td>' +
        '<td style="vertical-align:middle;">' + tipoproyecto + '</td>' +
        '<td style="vertical-align:middle;">' + roldescripcion + '</td>' +
        '<td><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn btn-primary btn-editar" style="margin-right: 5px;" ><i class="fa fa-edit"></i></button>' +

        '</div></td>' +
        '</tr>');

        html.find(".btn-editar").click(function () {

            /*$("#modaleditar input[name=id]").val(id);
            $("#modaleditar input[name=nombres]").val(nombre);
            $("#modaleditar input[name=apellidos]").val(apellidos);
            $("#modaleditar input[name=dni]").val(dni);
            $("#modaleditar input[name=email]").val(email);
            $("#modaleditar input[name=celular]").val(telefono);
            $("#modaleditar select[name=estado]").val(estado);*/

            $("#modaleditar").modal("show");

              //$("#modaleditar").modal("hide");
            $("#modaleditar .btn-eliminar").unbind();
            $("#modaleditar .btn-eliminar").click(function () {

                new Request("roles/eliminar/" + idusuario + "/" + id_proyecto + "/" + rol , {
                }, function (res) {
                    listar();
                    $("#modaleditar").modal("hide");
                });
            });

            


        });

    return html;
};


$(document).ready(function () {

    listar(1);
    //listarContacto();

    $(".filtros select[name=estado]").bind("change", function () {
        var est = $(this).val();
        listar(est);
    });

    /*new Request("proyecto/listar/",{
        estado: 1
    },function(res){
        console.log(res);
        $.each(res,function(k,v){
            var option = '<option value="'+v.pry_id_web+'">'+v.pry_descripcion+'</option>'
            $("select[name=proyecto]").append(option);
            // $("#modalagregar #cat_add select[name=categoria]").append(option);
        });

         //listar();
    });*/

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modalagregar").modal("hide");
                listar(1);
            }
        }
    }

    $("#modalagregar form").ajaxForm(options);

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modaleditar").modal("hide");
                listar(res.estado);
                $(".filtros select[name=estado]").val(res.estado);
            }
        }
    }
    $("#modaleditar form").ajaxForm(options);

    /*html.find(".btn-editar").click(function () {
        alert("HOLA");
        $("#modaleditar").modal("show");
    });*/

});



function listar(est) {
    console.log('log');
    $("#encargado .lista").empty();

    new Request("encargado/listar/", {
        estado: est
    }, function (res) {
        $("#encargado .lista").empty();
        $.each(res, function (k, v) {
            var it = new ItemEncargado(v);
            $("#encargado .lista").append(it);
        })

    });


}


var ItemEncargado = function (data) {

    var id = data.usr_id;
    var nombre = data.usr_nombres;
    var email = data.usr_email;
    var estado = data.usr_estado;
    var pass = data.usr_pass;
    var celular = data.usr_whatsapp;

    if(celular == null){
        celular = '----';
    }


    var html = $('<tr width="100%">' +
        '<td width="8%" style="vertical-align:middle;">' + id + '</td>' +
        '<td width="30%" style="vertical-align:middle;">' + nombre + '</td>' +
        '<td width="20%" style="vertical-align:middle;">' + email + '</td>' +
        '<td width="20%" style="vertical-align:middle;">' + celular + '</td>' +
        '<td><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn btn-primary btn-editar" style="margin-right: 5px;" ><i class="fa fa-edit"></i></button>' +
        '<a href="'+ path +'admin/designar/'+ id +'" class="btn btn-primary"><i class="fas fa-eye"></i> Proyectos</a>' +

        '</div></td>' +
        '</tr>');

        /*    var html = $('<tr width="100%" data-id="' + id + '">' +
                    '<td>' + data.pry_titulo + '</td>' +
                    '<td>' + data.pry_orden + '</td>' +
                    '<td><div class="btn-group" role="group" aria-label="...">' +
                    '<button type="button" class="btn btn-outline-primary btn-editar"><i class="fas fa-edit"></i> Editar</button>' +
                    '</div></td>' +
                '</tr>');*/



        html.find(".btn-editar").click(function () {

            $("#modaleditar input[name=id]").val(id);
            $("#modaleditar input[name=nombres]").val(nombre);
            $("#modaleditar input[name=email]").val(email);
            $("#modaleditar input[name=pass]").val(pass);
            $("#modaleditar select[name=estado]").val(estado);

            $("#modaleditar").modal("show");

            $("#modaleditar a.eliminar").unbind();
            $("#modaleditar a.eliminar").click(function () {
                $("#modaleditar").modal("hide");
                $("#modaleliminar .btn-eliminar").unbind();
                $("#modaleliminar .btn-eliminar").click(function () {

                    $("#modaleliminar").modal("show");
                    new Request("encargado/eliminar/" + id, {
                    }, function (res) {
                        listar(1);
                        $("#modaleliminar").modal("hide");
                    });
                });

            })


        });

    return html;
};




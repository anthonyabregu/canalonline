<?php
  class Roles extends CI_Controller{

    function __construct(){
      
      parent::__construct();
      $this->load->model('Roles_model', 'roles');

    }

    public function listar(){

      $usr_id = intval($this->input->get('usr_id'));

      $pry_id = intval($this->input->get('pry_id'));

      if($usr_id != "" && $pry_id == ""){ $lista = $this->roles->listar($usr_id); }

      if($pry_id != "" && $usr_id == ""){ $lista = $this->roles->listarPorProyecto($pry_id); }

      
      echo json_encode($lista);

    }


    public function agregar(){

      $param['usr_id'] = intval($this->input->post('encargado'));
      $param['pry_id'] = intval($this->input->post('proyecto'));
      $param['fnc_id'] = intval($this->input->post('rol'));

      $info = $this->roles->info($param['usr_id'], $param['pry_id'], $param['fnc_id']);

      if($info != false){
            
        $res["res"] = "YA EXISTE EL ROL INGRESADO";
        echo json_encode($res);

      }else{
            
        $result = $this->roles->agregar($param);

        if($result != false){
          $res["res"] = "ok";
          //$res["estado"] = $param['ctg_estado'];
          echo json_encode($res);
        }
      }
    }

    
    public function eliminar($usr_id, $pry_id, $fnc_id){
          
      /*if(!$this->session->has_userdata('admin')){
        exit();
      }*/
            
      $this->roles->eliminar($usr_id, $pry_id, $fnc_id);

      $res["res"] = "ok";
      echo json_encode($res);

    }

  }
?>
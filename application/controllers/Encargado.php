<?php
  class Encargado extends CI_Controller{

    function __construct(){
      parent::__construct();
      $this->load->model('Encargado_model', 'encargado');
    }

    public function index(){

      $data = array(
                "page" => "encargado"
            );

      $this->load->view('web/header', $data);
            $this->load->view('web/encargado');
            $this->load->view('web/footer');
    }

    public function listar(){

      $estado = intval($this->input->get('estado'));

      $lista = $this->encargado->listar($estado);
          echo json_encode($lista);
    }


    public function agregar(){

          $param['usr_nombres'] = $this->input->post('nombres');
          $param['usr_email'] = $this->input->post('email');

          $param['usr_pass'] = $this->input->post('pass');

          
          $result = $this->encargado->agregar($param);

          if($result != false){
            $res["res"] = "ok";
            //$res["estado"] = $param['ctg_estado'];
            echo json_encode($res);
          }
        }

        public function actualizar(){

            $id = intval($this->input->post('id'));

            $param['usr_nombres'] = $this->input->post('nombres');
            $param['usr_email'] = $this->input->post('email');

            $param['usr_pass'] = $this->input->post('pass');
            

            $param['usr_estado'] = intval($this->input->post('estado'));

            $editar = $this->encargado->editar($param, $id);

            if($editar != false){
              $res["res"] = "ok";
            $res["estado"] = $param['usr_estado'];
            echo json_encode($res);
            }
        }


        public function eliminar($id){
            
            /*if(!$this->session->has_userdata('admin')){
              exit();
            }*/
            
            $this->encargado->eliminar($id);


            $res["res"] = "ok";
            echo json_encode($res);
        }


        public function enviarcorreo(){

          $hostname = '{https://webmail.webfaction.com:993/pop3/novalidate-cert}INBOX';
          $mailserver = 'anthony@picnic.pe';
          $port = '110';
          $user = 'prueba_menorcaservices';
          $mailbox = '{imap.gmai.com:993/imap/ssl/novalidate-cert}INBOX';
          $pass = '12345678';

          
          /* try to connect */
          $inbox = imap_open($hostname,$user,$pass) or die('Cannot connect to Tiriyo: ' . imap_last_error());
          //echo $inbox;
          /* grab emails */
          $emails = imap_search($inbox,'ALL');


          /* if emails are returned, cycle through each... */
          if($emails) {

            /* begin output var */
            $output = '';

            /* put the newest emails on top */
            rsort($emails);

            /* for every email... */
            foreach($emails as $email_number) {
              //$email_number=$emails[0];
          //print_r($emails);
              /* get information specific to this email */
              $overview = imap_fetch_overview($inbox,$email_number,0);
              $message = imap_fetchbody($inbox,$email_number,2);

              /* output the email header information */
              $output.= '<div class="toggler '.($overview[0]->seen ? 'read' : 'unread').'">';
              $output.= '<span class="subject">'.$overview[0]->subject.'</span> ';
              $output.= '<span class="from">'.$overview[0]->from.'</span>';
              $output.= '<span class="date">on '.$overview[0]->date.'</span>';
              $output.= '</div>';

              /* output the email body */
              $output.= '<div class="body">'.$message.'</div>';
            }

            echo $output;
          }

          /* close the connection */
          imap_close($inbox);
        }


  

  }
?>
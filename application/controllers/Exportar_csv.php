<?php

	class Exportar_csv extends CI_Controller{

		function __construct(){

			parent::__construct();
			$this->load->model('Contacto_model', 'contacto');
		}


		public function contactos(){

            //$file_name = 'contactos'.date('Ymd').'.csv'; 

            $file_name = 'contactos_'.date('Ymd').'.csv'; 
            header("Content-Description: File Transfer"); 
            header("Content-Disposition: attachment; filename=$file_name"); 
            header("Content-Type: application/csv;");

            $pry_id = $this->input->post('proyecto');

            $s_rango = $this->input->post("daterange");

            $result = $this->contacto->exportar_csv($pry_id,$s_rango);

            if($result != false){
               
                // file creation 
                $file = fopen('php://output', 'w');

                $res["export"] = "ok";
                
                $header = array("NOMBRE","DNI","CELULAR","EMAIL","MENSAJE","CANAL","PROYECTO","FECHA REGISTRO"); 
                fputcsv($file, $header);

                foreach ($result->result_array() as $key => $value)
                { 
                	if($value["cnl_id"] == "w" || $value["cnl_id"] == "W" ){
                        $value["cnl_id"] = "WEB";
                    }

                    fputcsv($file, $value); 
                }
                fclose($file); 

            }else{
                $res["export"] = "failed";
            }

            return $res;
        }

	} 
?>
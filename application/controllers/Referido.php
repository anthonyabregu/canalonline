<?php

    use Restserver\Libraries\REST_Controller;
    require APPPATH . '/libraries/REST_Controller.php';
    require APPPATH . '/libraries/Format.php';

    class Referido extends REST_Controller{

        function __construct(){
            parent::__construct();
            $this->load->model('Referido_model', 'referido');
            //$this->load->model('Contacto_model', 'contacto');
            $this->load->library('correo');
            /*$this->load->library('REST_Controller');
            $this->load->library('Format');*/

        }

        

        public function listar_get(){

            $pry_id = intval($this->input->get('pry_id'));

            $lista = $this->referido->listar($pry_id);

            if($lista != false){
                $res["res"] = "ok";
                $res["lista"] = $lista;
            }else{
                $res["res"] = "No se encontraron referidos";
            }

             $this->response($res);
        }

        public function origen_get(){

        	$lista = $this->referido->listarOrigen();

            if($lista != false){
                $res["res"] = "ok";
                $res["lista"] = $lista;
            }else{
                $res["res"] = "No se encontraron referidos";
            }

        	 $this->response($res);
        }

        public function referenciados_get(){

            $orf_id = intval($this->input->get('orf_id'));

            $lista = $this->referido->referenciados($orf_id);

            if($lista != false){
                $res["res"] = "ok";
                $res["lista"] = $lista;
            }else{
                $res["res"] = "No se encontraron referenciados";
            }

             $this->response($res);
        }


        /*public function listar_contactos_get(){

            $pry_id = intval($this->input->get("pry_id"));

            $lista = $this->contacto->listar_contactos($pry_id);

            echo json_encode($lista);
        }*/

        public function agregar_post(){
          
            $jsonOrigen = $this->post('origen');
            $jsonRef = $this->post('referidos');

            //var_dump($jsonRef);
            
            // falta el parametro json de proyecto

            $origen['orf_nombre'] = $jsonOrigen["orf_nombre"];
            $origen['orf_tipo_documento'] = $jsonOrigen["orf_tipo_documento"];
            $origen['orf_n_documento'] = $jsonOrigen["orf_n_documento"];
            $origen['orf_celular'] = $jsonOrigen["orf_celular"];
            $origen['orf_email'] = $jsonOrigen["orf_email"];

            $origen['orf_terminos'] = $jsonOrigen["orf_terminos"];
            $origen['orf_politicas'] = $jsonOrigen["orf_politicas"];

            //$pry_id = $proyecto;

            $respuesta_origen = $this->referido->guardarOrigen($origen);

            $result = false;
            if($respuesta_origen != false){

                $nombres_referidos = $jsonRef["nombres"];
                $docu_referidos = $jsonRef["documentos"];
                $tipo_referidos = $jsonRef["tipos"];
                $cel_referidos = $jsonRef["celular"];
                $email_referidos = $jsonRef["email"];
                $proyecto_referidos = $jsonRef["idproyecto"];

                $result = $this->referido->guardarReferidos(
                    $nombres_referidos,
                    $docu_referidos,
                    $tipo_referidos,
                    $cel_referidos,
                    $email_referidos,
                    $respuesta_origen,
                    $proyecto_referidos
                );

                if($result != false){

                    $pry_id = $jsonRef["idproyecto"];

                    $rolusuario = 3;

                    for ($i=0; $i < count($nombres_referidos) ; $i++) {
                        
                        //DESIGNAR CORREO A RECEPTORES
                        $usuario = $this->referido->encargadoReferido($pry_id[$i], $rolusuario);

                        if($usuario != false){
                            foreach($usuario as $user){

                                $param['nombres']=$nombres_referidos[$i];
                                $param['documentos']=$docu_referidos[$i];
                                $param['tipos']=$tipo_referidos[$i];
                                $param['celular']=$cel_referidos[$i];
                                $param['email']=$email_referidos[$i];

                                $correo = $this->correoReferidos($param, $origen, $user);
                            }

                            if($correo != false){
                                $res["res"] = "ok";
                                //$res['correo'] = $correo;
                            }else{
                                $res["res"] = "ok";
                                $res['correo'] = 'No se envió el correo';
                            }
                        }else{
                            $res["res"] = "NO SE ENCONTRO AL ENCARGADO DEL PROYECTO";
                            $res['correo'] = 'No se envió el correo';
                        }
                    }

                }else{
                    $res['res'] = "No se pudo agregar el referenciados";
                    $res['correo'] = 'No se envió el correo';
                }   

            }else{
                $res['res'] = "No se pudo agregar el referido (cliente)";
                $res['correo'] = 'No se envió el correo';
            }

            $this->response($res); 

        }


        public function correoReferidos($param, $origen, $user){

            //var_dump($param);
            $fechahora = date("Y-m-d H:i:s");

            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'contacto@menorca.com.pe',
                'smtp_pass' => 'menorca123',
                'mailtype'  => 'html',
                'charset'   => 'utf-8'
            );
            $this->load->library('email', $config);
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");

            // Set to, from, message, etc.
            $this->email->from('contacto@menorca.com.pe', "Menorca");

            $this->email->to($user->email);


            $this->email->reply_to($user->email);


            $this->email->subject($user->proyecto." | menorca.pe - Referidos");

            $txt_mensaje = '';

            $documentoOrigen = $this->documento($origen["orf_tipo_documento"]);


            $txt_mensaje = $txt_mensaje . '
                            <style>
                                table tr td{
                                    background-color:#fff;
                                    font-family:Tahoma;
                                    font-size:12px
                                }
                            </style>
                            <p><strong>Proyecto:</strong> '.$user->proyecto.'</p></br>
                            <table width="400" border="0" cellpadding="10" cellspacing="1" bgcolor="#ccc">
                                <tr>
                                    <td colspan="2" bgcolor="#fff"><center><strong>FORMULARIO DE REFERIDOS DESDE LA WEB</strong></center></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#fff"><strong>Cliente:</strong></td>
                                    <td bgcolor="#fff">
                                        Nombre: '.$origen["orf_nombre"].'<br>
                                        N° Documento: '.$origen["orf_n_documento"].'<br>
                                        Celular: '.$origen["orf_celular"].'<br>
                                        Email: '.$origen["orf_email"].'<br>
                                    </td>
                                </tr>';

            

                //$cont = $i + 1;

                //var_dump($param['nombres']);

                $documentoRef = $this->documento($param['tipos']);

                $txt_mensaje = $txt_mensaje .   '<tr>
                                                    <td bgcolor="#fff"><strong>Referido:</strong></td>
                                                    <td bgcolor="#fff">
                                                        Nombre: '.$param['nombres'].'<br>
                                                        N° Documento: '.$param['documentos'].'<br>
                                                        Celular: '.$param['celular'].'<br>
                                                        Email: '.$param['email'].'<br>
                                                    </td>
                                                </tr>';
            

            /*for ($i=0; $i < count($param['nombres']) ; $i++) {

                $cont = $i + 1;

                $documentoRef = $this->documento($param['tipos'][$i]);

                $txt_mensaje = $txt_mensaje .   '<tr>
                                                    <td bgcolor="#fff"><strong>Referido '.$cont.' :</strong></td>
                                                    <td bgcolor="#fff">
                                                        Nombre: '.$param['nombres'][$i].'<br>
                                                        Tipo Documento: '.$documentoRef.'<br>
                                                        N° Documento: '.$param['documentos'][$i].'<br>
                                                        Celular: '.$param['celular'][$i].'<br>
                                                        Email: '.$param['email'][$i].'<br>
                                                    </td>
                                                </tr>';
            }*/

            $txt_mensaje = $txt_mensaje . '</table>';

            $this->email->message($txt_mensaje);

            $result = $this->email->send();

            //return $user->email . "" .$txt_mensaje;


            if($result != false){
              $res["envio_correo"] = "ok";
            }else{
              $res["envio_correo"] = "failed";
            }

            return $result;


        }

        /*public function actualizar(){

            $id = intval($this->input->post('id'));
            $param['cto_nombre'] = $this->input->post('nombre');
            $param['cto_dni'] = $this->input->post('dni');
            $param['cto_celular'] = $this->input->post('celular');
            $param['cto_email'] = $this->input->post('email');
            $param['pry_id'] = intval($this->input->post('proyecto'));
            $param['cto_mensaje'] = $this->input->post('mensaje');
            $param['cnl_id'] = $this->input->post('canal');

            $editar = $this->contacto->editar($param, $id);

            if($editar != false){
              $res["res"] = "ok";
  		      echo json_encode($res);
            }
        }*/


        public function documento($tipo){

            switch ($tipo) {
                case '1':
                    $tipodocumento = "Dni";
                    break;

                case '2':
                    $tipodocumento = "Carnet de Extranjería";
                    break;
                
                default:
                    $tipodocumento = "Sin especificar";
                    break;
            }

            return $tipodocumento;

        }

        
    }
?>

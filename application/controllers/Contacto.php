﻿<?php

    use Restserver\Libraries\REST_Controller;
    require APPPATH . '/libraries/REST_Controller.php';
    require APPPATH . '/libraries/Format.php';

    class Contacto extends REST_Controller{

        function __construct(){
            parent::__construct();
            $this->load->model('Contacto_model', 'contacto');
            /*$this->load->library('REST_Controller');
            $this->load->library('Format');*/

        }

        public function listar_get(){

            $pry_id = intval($this->input->get("pry_id"));
            $usr_id = intval($this->input->get("usr_id"));
            //$usr_tipo = intval($this->input->get("usr_tipo"));
            $usr_tipo = $this->input->get("usr_tipo");
            $s_rango = $this->input->get("rango");

            //var_dump($usr_tipo);

            $pg = $this->input->get("pg");
            $cant = $this->input->get("cant");

            $limit = "";
            if($pg!=0 && $cant!=0){
                $ini = ($pg - 1)*$cant;
                $limit = " LIMIT ".$ini.",".$cant;
            }

            if($pry_id == ""){

                $data = $this->contacto->listarPorFecha($usr_id, $usr_tipo, $s_rango, $limit);

            }else{
                $data = $this->contacto->listarPorFiltros($pry_id, $usr_id, $usr_tipo, $s_rango, $limit);
                
            }

            //$lista = $this->contacto->listar_contactos($pry_id, $usr_id, $usr_tipo, $s_rango);

            if($data != false){
                $res["res"] = "ok";
                $res["lista"] = $data["data"];
                $res["total"] = $data["total"];
            }else{
                $res["res"] = "failed";
            }

             $this->response($res);
        }

        public function agregar_post(){
          
            $json = $this->post('par');
            
            $param['cnt_nombre'] = $json["nombre"];
            $param['cnt_documento'] = 1;
            $param['cnt_nro_documento'] = $json["dni"];
            $param['cnt_celular'] = $json["celular"];
            $param['cnt_email'] = $json["email"];
            $param['pry_id'] = intval($json["proyecto"]);
            $param['cnt_canal'] = $json["canal"];
            $param['cnt_captacion'] = $json["captacion"];
            $param['cnt_leadsweb'] = $json["tipoleads"];

            /* CAMPOS ADICIONALES SI EL LEADS VIENE DEL BLOG */
            if($param['leadsweb'] == 'blog'){
                $param['cnt_blogalias'] = $json["blog"];
                $param['cnt_documento'] = intval($json["tipodocumento"]);
                $usuario = false;
            }
            
            if($param['leadsweb'] == 'proyecto'){
                $param['cnt_mensaje'] = $json["mensaje"];
                $param['cnt_term'] = intval($json["terminos"]);
                $param['cnt_interes'] = $json["interes"];
                $param['cnt_politica'] = intval($json["politica"]);

                $rolusuario = 2;
                //DESIGNAR CORREO A RECEPTORES
                $usuario = $this->contacto->usuario_proyecto($param['pry_id'], $rolusuario);
            }
            
            
            if($usuario != false){
              foreach($usuario as $user){
                  $correo = $this->enviar($param, $user);
              }
            }else{
                //var_dump("NO SE ENCONTRO USUARIO RECEPTOR");
            }


            $result = $this->contacto->agregar($param);

            if($result != false){
                $res["res"] = "ok";
                
            }else{
                 $res["res"] = false;
            }

            $this->response($res);
        }

        public function actualizar(){

            $id = intval($this->input->post('id'));
            $param['cnt_nombre'] = $this->input->post('nombre');
            $param['cnt_nro_documento'] = $this->input->post('dni');
            $param['cnt_celular'] = $this->input->post('celular');
            $param['cnt_email'] = $this->input->post('email');
            $param['pry_id'] = intval($this->input->post('proyecto'));
            $param['cnt_mensaje'] = $this->input->post('mensaje');
            $param['cnt_canal'] = $this->input->post('canal');

            $editar = $this->contacto->editar($param, $id);

            if($editar != false){
              $res["res"] = "ok";
  		        //$res["estado"] = $param['ctg_estado'];
  		       $this->response($res);
            }
        }

        public function enviar($param, $user){

            /*$nombres = $this->input->post("nombres");
            $dni = $this->input->post("dni");
            $celular = $this->input->post("celular");
            $email = $this->input->post("email");
            $mensaje = $this->input->post("mensaje");
            $proyecto = $this->input->post("proyecto");*/

            $fechahora = date("Y-m-d H:i:s");

            $data = array(
                "nombre" => $param['cnt_nombre'],
                "dni" => $param['cnt_nro_documento'],
                "celular" => $param['cnt_celular'],
                "email" => $param['cnt_email'],
                "mensaje" => $param['cnt_mensaje'],
                "proyecto" => $user->proyecto,
                "registro" => $fechahora
            );

            //$this->contacto->agregar($data);

            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'contacto@menorca.com.pe',
                'smtp_pass' => 'menorca123',
                'mailtype'  => 'html',
                'charset'   => 'utf-8'
            );
            $this->load->library('email', $config);

            $this->email->initialize($config);
            
            $this->email->set_newline("\r\n");

            // Set to, from, message, etc.
            $this->email->from('contacto@menorca.com.pe', "Menorca");

            $this->email->to($user->email);


            $this->email->reply_to($user->email);


            $this->email->subject($user->proyecto." | menorca.pe - Coordina tu visita");


            $txt_mensaje = '
                <style>
                    table tr td{
                        background-color:#fff;
                        font-family:Tahoma;
                        font-size:12px
                    }
                </style>
                <table width="400" border="0" cellpadding="10" cellspacing="1" bgcolor="#ccc">
                    <tr>
                        <td colspan="2" bgcolor="#fff"><center><strong>MENSAJE DE CONTACTO DESDE LA WEB</strong></center></td>
                    </tr>
                    <tr>
                        <td bgcolor="#fff"><strong>Proyecto:</strong></td>
                        <td bgcolor="#fff">'.$user->proyecto.'</td>
                    </tr>
                    <tr>
                        <td bgcolor="#fff"><strong>Nombres</strong></td>
                        <td bgcolor="#fff">'.$data["nombre"].'</td>
                    </tr>
                    <tr>
                        <td bgcolor="#fff"><strong>DNI:</strong></td>
                        <td bgcolor="#fff">'.$data["dni"].'</td>
                    </tr>
                    <tr>
                        <td bgcolor="#fff"><strong>Tel&eacute;fono:</strong></td>
                        <td bgcolor="#fff">'.$data["celular"].'</td>
                    </tr>
                    <tr>
                        <td bgcolor="#fff"><strong>Email:</strong></td>
                        <td bgcolor="#fff">'.$data["email"].'</td>
                    </tr>

                    <tr>
                        <td bgcolor="#fff"><strong>Mensaje</strong>:</td>
                        <td bgcolor="#fff">'.str_replace("\n","<br>",$data["mensaje"]).'</td>
                    </tr>
                </table>';


            $this->email->message($txt_mensaje);

            $result = $this->email->send();

            //return $user->email . "" .$txt_mensaje;


            if($result != false){
              $res["envio_correo"] = "ok";
            }else{
              $res["envio_correo"] = "failed";
            }

            return $res;


        }

        public function dashboard_contacto_get(){


            $_rango = $this->input->get("rango");

            $lista = $this->contacto->dashboard_contacto($_rango);

            if($lista != false){
              $res["res"] = "ok";
              $res["lista"] = $lista;
            }else{
              $res["res"] = "failed";
            }

             $this->response($res);
        }

    }
?>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class Administrador extends CI_Controller {


    public function __construct(){
        parent::__construct();
        $this->load->model('Administrador_model','admin');
        $this->load->library('session');
        //$this->load->library('email');
    }

    public function login(){

        $usuario = $this->input->post("usuario");
        $clave = $this->input->post("clave");

        $result = $this->admin->login($usuario, $clave);

        if($result["respuesta"] == 1){

            redirect("admin");

        }else{

            redirect("admin/login?error=error");
        }

        /*if($usuario == "admin" && $clave == "adminmenorca"){

            $res = (object) array(
                "login" => "ok",
                "usuario" => "camet"
            );
            $this->session->set_userdata('admin',$res);

            redirect("admin");

        }else{
            redirect("admin/login?error=error");
        }*/



    }

    public function logout(){

        $this->session->unset_userdata('idUsuario');
        $this->session->unset_userdata('tipoUsuario');
        $this->session->unset_userdata('Conexion');
        $this->session->sess_destroy();
        redirect("admin/login");
    }




}

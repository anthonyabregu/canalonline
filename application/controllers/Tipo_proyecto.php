<?php
	class Tipo_proyecto extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->model('Tipo_proyecto_model', 'tipo');
		}

		public function index(){

			$data = array(
                "page" => "tipoproyecto"
            );

			$this->load->view('web/header', $data);
            $this->load->view('web/tipo_proyecto');
            $this->load->view('web/footer');
		}


		public function listar(){

			$estado = intval($this->input->get('estado'));

			$lista = $this->tipo->listar($estado);
        	
        	if($lista != false){
        		$res = $lista;
        		echo json_encode($res);
        	}else{
        		$res = "empty";
        		echo json_encode($res);
        	}
		}


		public function agregar(){

          $param['tip_descripcion'] = $this->input->post('tipoproyecto');


          $result = $this->tipo->agregar($param);

          if($result != false){
            $res["res"] = "ok";
            //$res["estado"] = $param['ctg_estado'];
            echo json_encode($res);
          }
        }

        public function actualizar(){

            $id = intval($this->input->post('id'));

            $param['tip_descripcion'] = $this->input->post('tipoproyecto');
            $param['tip_estado'] = $this->input->post('estado');
          	

            $editar = $this->tipo->editar($param, $id);

            if($editar != false){
              $res["res"] = "ok";
  		      $res["estado"] = $param['tip_estado'];
  		      echo json_encode($res);
            }
        }


        public function eliminar($id){
            
            /*if(!$this->session->has_userdata('admin')){
              exit();
            }*/
            
            $this->tipo->eliminar($id);


            $res["res"] = "ok";
            echo json_encode($res);
        }

	}
?>
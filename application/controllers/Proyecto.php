<?php
	class Proyecto extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->model('Proyecto_model', 'proyecto');
		}

		public function index(){

			$data = array(
                "page" => "proyecto"
            );

			$this->load->view('web/header', $data);
            $this->load->view('web/proyectos');
            $this->load->view('web/footer');
		}


		public function listado(){

			$estado = intval($this->input->get('estado'));
      $usr_id = intval($this->input->get('usr_id'));
      $rol = intval($this->input->get('rol'));

      $usr_tipo= $this->input->get('usr_tipo'); // array

			$lista = $this->proyecto->listado($estado, $usr_id, $usr_tipo, $rol);
        	
        	if($lista != false){
        		$res = $lista;
        		echo json_encode($res);
        	}else{
        		$res = "empty";
        		echo json_encode($res);
        	}
		}

		public function listar(){

          /*if(!$this->session->has_userdata('admin')){
  			       exit();
          }*/

          $estado = intval($this->input->get("estado"));
          $tipo = intval($this->input->get("tipo"));


      		$lista = $this->proyecto->listar($estado,$tipo);
          

          if( $lista != false )
          {
            echo json_encode($lista);
          }
          else{
            $res = 'empty';
            echo json_encode($res);
          }

        }


		public function agregar(){

          $param['pry_descripcion'] = $this->input->post('proyecto');
          $param['tip_id'] = intval($this->input->post('tipoproyecto'));
          $param['ubg_id'] = intval($this->input->post('ubigeo'));


          $result = $this->proyecto->agregar($param);

          if($result != false){
            $res["res"] = "ok";
            //$res["estado"] = $param['ctg_estado'];
            echo json_encode($res);
          }
        }

        public function actualizar(){

            $id = intval($this->input->post('id'));

            $param['pry_descripcion'] = $this->input->post('proyecto');
	        $param['tip_id'] = intval($this->input->post('tipoproyecto'));
	        $param['ubg_id'] = intval($this->input->post('ubigeo'));
            $param['pry_estado'] = intval($this->input->post('estado'));
          	

            $editar = $this->proyecto->editar($param, $id);

            if($editar != false){
              $res["res"] = "ok";
  		      $res["estado"] = $param['pry_estado'];
  		      echo json_encode($res);
            }
        }


        public function eliminar($id){
            
            /*if(!$this->session->has_userdata('admin')){
              exit();
            }*/
            
            $this->proyecto->eliminar($id);


            $res["res"] = "ok";
            echo json_encode($res);
        }

	}
?>
<?php $tipoUsuario = $this->session->userdata('tipoUsuario'); ?>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/home.css?v=1.4">

<div class="container">
  <h1>Dashboard</h1>
  <label>Filtrado por Fecha:</label>
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
         <input type="text" class="form-control" name="daterange" value="07/05/2019 - 08/05/2019" />
      </div>
    </div>
    <div class="col-md-3">
      <button type="button" id="btn-reporte" class="btn btn-success"><i class="fas fa-chart-pie"></i> Generar Reporte</button>
    </div>
  </div>
  <div class="row">
    <div class="col-md-5">
      <div class="card-box">
        <table class="table table-striped">
          <thead class="thead-dark">
            <tr>
              <th>PROYECTO</th>
              <th>CANTIDAD</th>
              <th>PORCENTAJE</th>
            </tr>
          </thead>
          <tbody class="lista_contacto">
          </tbody>
        </table>
      </div>
    </div>
    <div class="col-md-7">
      <div id="graph-contacto" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
    </div>
    
  </div>
  <br><br>
  <div class="row">

    <?php if(in_array(1, $tipoUsuario)){ ?>
      <div class="col-sm-3">
          <a class="item" href="<?php echo base_url(); ?>admin/contacto">
              <div class="icono">
                  <i class="fa fa-address-book icono"></i>
              </div>
              <div class="nom">Contactos</div>
          </a>
      </div>
      <div class="col-sm-3">
          <a class="item" href="<?php echo base_url(); ?>admin/proyectos">
              <div class="icono"><i class="fas fa-project-diagram icono"></i></div>
              <div class="nom">Proyectos</div>
          </a>
      </div>
      <div class="col-sm-3">
          <a class="item" href="<?php echo base_url(); ?>admin/tipo_proyecto">
              <div class="icono"><i class="fas fa-layer-group icono"></i></div>
              <div class="nom">Tipo Proyecto</div>
          </a>
      </div>
      <div class="col-sm-3">
          <a class="item" href="<?php echo base_url(); ?>admin/Encargado">
              <div class="icono">
                  <i class="fa fa-user icono"></i>
              </div>
              <div class="nom">Encargados</div>
          </a>
      </div>

      <!--<div class="col-sm-3">
          <a class="item" href="<?php echo base_url(); ?>admin/usuariowsp">
            <div class="icono">
              <i class="fab fa-whatsapp icono"></i>
            </div>
            <div class="nom">Consultas Whatsapp</div>
          </a>
      </div> -->

    
    <?php } ?>

    <?php if(in_array(2, $tipoUsuario)){?>
      <div class="col-sm-3">
          <a class="item" href="<?php echo base_url(); ?>admin/contacto">
              <div class="icono">
                  <i class="fa fa-address-book icono"></i>
              </div>
              <div class="nom">Contactos</div>
          </a>
      </div>
    <?php } ?>

    

    <?php if(in_array(3, $tipoUsuario)){?>
      <div class="col-sm-3">
          <a class="item" href="<?php echo base_url(); ?>admin/origen_referencia">
              <div class="icono">
                  <i class="fas fa-user-friends icono"></i>
              </div>
              <div class="nom">Referidos</div>
          </a>
      </div>
    <?php } ?>

    <?php if(in_array(4, $tipoUsuario)){?>
      <!--<div class="col-sm-3">
          <a class="item" href="<?php echo base_url(); ?>admin/usuariowsp">
            <div class="icono">
              <i class="fab fa-whatsapp icono"></i>
            </div>
            <div class="nom">Consultas Whatsapp</div>
          </a>
      </div> -->
    <?php } ?>
    
    
  </div>
</div>

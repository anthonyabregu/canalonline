<link rel="stylesheet" href="<?php echo base_url(); ?>assets/web/css/contacto.css">

<div class="container" id="referido">
    <h1 class="page-title">Referencia (Origen)</h1>
    <br>
    <button type="button" class="btn btn-success btn-agregar btn-trans" data-toggle="modal" data-target="#modalagregar" hidden><i class="fas fa-plus-circle"></i> Agregar</button>
    <br>
    <div class="row filtros">
        <div class="col-sm-7">
            <div class="card-box">
                <form method="POST" action="<?php echo base_url(); ?>exportar_csv/referidos">
                    <label>Proyectos</label>
                    <div class="form-inline">
                        <select class="form-control" name="proyecto">
                            <option value="">Todos</option>
                        </select>
                        <button type="submit" class="btn btn-success boton-export">Exportar en CSV</button>
                    </div>
                </form>
            </div>
        </div>
    </div><br>
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Teléfono</th>
                            <th width="150">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="lista">

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="modalagregar">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">Agregar Contacto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo base_url(); ?>contacto/agregar" method="post" autocomplete="off">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">NOMBRE</label>
                            <input type="text" name="nombre" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">DNI</label>
                            <input type="text" name="dni" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">CELULAR</label>
                            <input type="text" name="celular" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">EMAIL</label>
                            <input type="email" name="email" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">PROYECTO</label>
                            <select class="form-control" name="proyecto" required>
                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">MENSAJE</label>
                            <textarea name="mensaje" rows="8" cols="80" class="form-control" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">CANAL</label>
                            <select class="form-control" name="canal" required>
                                <option value="W">WEB</option>
                                <option value="F">FACEBOOK</option>
                            </select>
                        </div>
                        <div class="form-inline">
                            <input type="checkbox" name="terminos" value="1" required>
                            <label for="" style="margin-left: 10px;"> Acepto los términos y condiciones</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Agregar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="modaleditar">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">Ver Contacto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo base_url(); ?>contacto/actualizar" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                        <label>ID</label>
                        <input type="text" name="id" class="form-control" readonly>
                    </div>
                        <div class="form-group">
                            <label for="">NOMBRE</label>
                            <input type="text" name="nombre" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">APELLIDOS</label>
                            <input type="text" name="apellido" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">DNI</label>
                            <input type="text" name="dni" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">CELULAR</label>
                            <input type="text" name="celular" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">EMAIL</label>
                            <input type="email" name="email" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">PROYECTO</label>
                            <select class="form-control" name="proyecto" required>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">MENSAJE</label>
                            <textarea name="mensaje" rows="8" cols="80" class="form-control" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">CANAL</label>
                            <select class="form-control" name="canal" required>
                                <option value="W">WEB</option>
                                <option value="F">FACEBOOK</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a class="eliminar" href="#modaleliminar" data-toggle="modal" style="position:absolute; left:15px; color:red">Eliminar</a>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>


<script type="text/javascript">
    var encargado = <?php echo $encargado?>;
</script>


<div class="container" id="designar">
    <h1 class="page-title">Designar Proyecto</h1>
    <br>
    <button type="button" class="btn btn-success btn-agregar btn-trans" data-toggle="modal" data-target="#modalagregar" ><i class="fas fa-plus-circle"></i> Agregar</button>
    <br>
    <br>
    <div class="row filtros">
        <div class="col-sm-3" hidden>
            <div class="card-box">
                <label>Estado</label>
                <select class="form-control" name="estado">
                    <option value="1" selected>Habilitados</option>
                    <option value="0">Deshabilitados</option>
                </select>
            </div>
        </div>
    </div><br>
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Proyecto</th>
                            <th>Tipo de Proyecto</th>
                            <th>Rol del Encargado</th>
                            <th width="150">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="lista">

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="modalagregar">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">Escoger Proyecto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo base_url(); ?>roles/agregar" method="post" autocomplete="off">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>ID. ENCARGADO</label>
                            <input type="text" name="encargado" class="form-control" readonly="">
                        </div>
                        <div class="form-group">
                            <label for="">PROYECTO</label>
                            <select class="form-control" name="proyecto" required>
                                <option value="" selected>Seleccione...</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">ROLES</label>
                            <select class="form-control" name="rol" required>
                                <option value="" selected>Seleccione...</option>
                                <option value="2">Contactos</option>
                                <option value="3">Referidos</option>
                                <option value="4">Whatsapp</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Agregar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">Ver Contacto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo base_url(); ?>roles/actualizar" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>ID. ENCARGADO</label>
                            <input type="text" name="encargado" class="form-control" readonly="">
                        </div>
                        <div class="form-group">
                            <label for="">PROYECTO</label>
                            <select class="form-control" name="proyecto" required>
                                
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a class="eliminar" href="#modaleliminar" data-toggle="modal" style="position:absolute; left:15px; color:red">Eliminar</a>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <div class="modal" tabindex="-1" role="dialog" id="modaleditar">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">¡Alerta!</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body">
                    <p>¿Está seguro que desea eliminar este proyecto?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-eliminar">Eliminar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>


</div>


<style type="text/css">
    .btn-origen{
        margin-left: 5px !important;
    }
</style>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/web/css/contacto.css">

<div class="container" id="referido">
    <h1 class="page-title">Referidos</h1>
    <br>
    <button type="button" class="btn btn-success btn-agregar btn-trans" data-toggle="modal" data-target="#modalagregar" hidden><i class="fas fa-plus-circle"></i> Agregar</button>
    <br>
    <div class="row filtros">
        <div class="col-sm-7">
            <div class="card-box">
                <form method="POST" action="<?php echo base_url(); ?>exportar_csv/referidos">
                    <label>Proyectos</label>
                    <div class="form-inline">
                        <select class="form-control" name="proyecto">
                            <option value="">Todos</option>
                        </select>
                        <button type="submit" class="btn btn-success boton-export">Exportar en CSV</button>
                    </div>
                </form>
            </div>
        </div>
    </div><br>
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Teléfono</th>
                            <th>Proyecto</th>
                            <th width="150">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="lista">

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    
    <div class="modal" tabindex="-1" role="dialog" id="modaleditar">
        <div class="modal-dialog" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">Ver Referido</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                        <label>ID</label>
                        <input type="text" name="id" class="form-control" readonly>
                    </div>
                        <div class="form-group">
                            <label for="">NOMBRE</label>
                            <input type="text" name="nombre" value="" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">TIPO DOCUMENTO</label>
                            <input type="text" name="tipo_documento" value="" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">DNI</label>
                            <input type="text" name="dni" value="" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">CELULAR</label>
                            <input type="text" name="celular" value="" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">EMAIL</label>
                            <input type="email" name="email" value="" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">PROYECTO</label>
                            <input type="text" name="proyecto" value="" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="modalorigen">
        <div class="modal-dialog" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">Ver Origen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                        <label>ID</label>
                        <input type="text" name="id" class="form-control" readonly>
                    </div>
                        <div class="form-group">
                            <label for="">NOMBRE</label>
                            <input type="text" name="nombre" value="" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">TIPO DOCUMENTO</label>
                            <input type="text" name="tipo_documento" value="" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">DNI</label>
                            <input type="text" name="dni" value="" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">CELULAR</label>
                            <input type="text" name="celular" value="" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">EMAIL</label>
                            <input type="email" name="email" value="" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>

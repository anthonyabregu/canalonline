
<div class="container" id="tipo">
    <h1 class="page-title">Tipo Proyectos</h1>
    <br>
    <button type="button" class="btn btn-success btn-agregar btn-trans" data-toggle="modal" data-target="#modalagregar" ><i class="fas fa-plus-circle"></i> Agregar</button>
    <br>
    <br>
    <div class="row filtros" >
        <div class="col-sm-3">
            <div class="card-box">
                <label>Estado</label>
                <select class="form-control" name="estado">
                    <option value="1" selected>Habilitados</option>
                    <option value="0">Deshabilitados</option>
                </select>
            </div>
        </div>
    </div><br>
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Descripción</th>
                            <th width="150">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="lista">

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="modalagregar">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">Agregar Tipo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo base_url(); ?>tipo_proyecto/agregar" method="post" autocomplete="off">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">TIPO PROYECTO</label>
                            <input type="text" class="form-control" name="tipoproyecto" required> 
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Agregar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="modaleditar">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">Ver Tipo Proyecto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo base_url(); ?>tipo_proyecto/actualizar" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>ID</label>
                            <input type="text" name="id" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">TIPO PROYECTO</label>
                            <input type="text" class="form-control" name="tipoproyecto" required> 
                        </div>  
                        <div class="form-group">
                            <label for="">ESTADO</label>
                            <select class="form-control" name="estado" required>
                                <option value="1">Habilitado</option>
                                <option value="0">Deshabilitado</option>
                            </select>
                        </div>                      
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="modaleliminar">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">¡Alerta!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>¿Está seguro que desea eliminar este Tipo de Proyecto?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-eliminar">Eliminar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>



</div>

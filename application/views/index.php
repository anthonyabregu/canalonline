<!DOCTYPE html>
<html lang="es" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Matrícula | Alumnos</title>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/libs/bootstrap/css/bootstrap.min.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="container"><br>
            <p>Lista de Alumnos</p>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                        </tr>
                    </thead>
                    <tbody id="tb_alumnos">
                    </tbody>
                </table>
            </div>
        </div>

        <script type="text/javascript">
            var baseurl =  "<?php echo base_url();?>";
        </script>

        <script src="<?php echo base_url();?>assets/libs/bootstrap/js/bootstrap.min.js"></script>

        <script src="<?php echo base_url();?>assets/alumno/js/alumno.js"></script>
    </body>
</html>

<?php

	class Ubigeo_model extends CI_Model{

		function __construct(){

			parent::__construct();
		}


        public function listar(){

            $sql = "select * from ubigeo order by ubg_departamento asc, ubg_provincia asc, ubg_distrito asc";
            $query =  $this->db->query($sql);
           
            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }


        public function info($id){

            $query = $this->db->get_where("ubigeo",array("ubg_id" => $id));
            if($query->num_rows()==0) return null;
            else return $query->first_row();
        }


        

        
	}
?>
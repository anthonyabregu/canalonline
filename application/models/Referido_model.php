<?php
  class Referido_model extends CI_Model{

        function __construct(){
            parent::__construct();
        }

        public function listar($pry_id){

            $tipoUsuario = $this->session->userdata('tipoUsuario');

            if(in_array(1, $tipoUsuario)){

                if($pry_id == ""){

                    $sql = "SELECT rfn.*, orf.*, pry.pry_descripcion from referenciados as rfn inner join origen_referencia as orf on rfn.orf_id = orf.orf_id inner join proyecto as pry on 
                        rfn.pry_id = pry.pry_id";

                    $query = $this->db->query($sql);

                }else{

                    $sql = "SELECT rfn.*, orf.*, pry.pry_descripcion from referenciados as rfn inner join origen_referencia as orf on rfn.orf_id = orf.orf_id inner join proyecto as pry on 
                        rfn.pry_id = pry.pry_id WHERE rfn.pry_id = ? ";

                    $query = $this->db->query($sql, array($pry_id));
                }

                 
            }else{

                if($pry_id == ""){

                $sql = "SELECT rfn.*, orf.*, pry.pry_descripcion from referenciados as rfn inner join origen_referencia as orf on rfn.orf_id = orf.orf_id inner join proyecto as pry on 
                        rfn.pry_id = pry.pry_id inner join roles as rls on pry.pry_id = rls.pry_id 
                        where rls.fnc_id = ? ";

                $query = $this->db->query($sql, array(3));

                }else{
                    $sql = "SELECT rfn.*, orf.*, pry.pry_descripcion from referenciados as rfn inner join origen_referencia as orf on rfn.orf_id = orf.orf_id inner join proyecto as pry on 
                            rfn.pry_id = pry.pry_id inner join roles as rls on pry.pry_id = rls.pry_id
                            WHERE rfn.pry_id = ? AND rls.fnc_id = ? ";

                    $query = $this->db->query($sql, array($pry_id, 3));
                }
            
            }

            //$query = $this->db->query($sql);

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }

        public function listarOrigen(){

            $this->db->order_by('orf_id', 'ASC');
            $query = $this->db->get('origen_referencia');

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }

        public function referenciados($orf_id){

            $this->db->order_by('rfn_id', 'ASC');
            $query = $this->db->get_where('referenciados', array('orf_id' => $orf_id));

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }


        public function guardarOrigen($origen){
            // var_dump($origen);
            if($this->db->insert('origen_referencia', $origen))
            {
                $sql = "SELECT MAX(orf_id) AS id FROM origen_referencia";
                
                $query = $this->db->query($sql);
                if($query->num_rows()>0){
                

                    $origen = $query->first_row();

                    return $origen->id;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }

        }

        public function origen_existe($doc){
            
            $sql = "SELECT * FROM origen_referencia WHERE orf_n_documento = ? ";
                
            $query = $this->db->query($sql, array($doc));
            if($query->num_rows()>0){
                
                $origen = $query->first_row();
                return $origen->orf_id;
            }
            else{
                return false;
            }
        }

        public function guardarReferidos($nombres, $docu, $tipo, $celular, $email, $id, $pry_id){

            $result = false;

            $param['orf_id'] = $id;
            for ($i=0; $i < count($nombres) ; $i++) {
                $param['rfn_nombres'] = $nombres[$i];
                $param['rfn_tipo_documento'] = $tipo[$i];
                $param['rfn_n_documento'] = $docu[$i];
                $param['rfn_celular'] = $celular[$i];
                $param['rfn_email'] = $email[$i];
                $param['pry_id'] = $pry_id[$i];
                $result = $this->db->insert('referenciados', $param);
            }

            return $result;

        }

       public function consultarorigen($documento){

            $sql = "SELECT orf_nombre,orf_tipo_documento,orf_celular,orf_email FROM origen_referencia where orf_n_documento= ? ";

            $query = $this->db->query($sql, array($documento));

            if($query->num_rows()>0){

                return $query->first_row();
            }
            else{
                return false;
            }
       }


        /*public function usuario_proyecto($pry_id){

            $sql = "SELECT usw.usw_nombres as 'nombres', usw.usw_apellidos as 'apellidos',  pry.pry_id as 'id_proyecto',
            pry.pry_descripcion as 'proyecto' from usuario_whatsapp as usw inner join usw_proyectos as usrpry on usw.usw_id = usrpry.usw_id
            inner join proyecto as pry on usrpry.pry_id = pry.pry_id WHERE pry.pry_id = ? AND usrpry.uswpry_estado = ? ";

            $query = $this->db->query($sql, array($pry_id, 1));

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }*/


        public function encargadoReferido($pry_id, $rol){

            $sql = "SELECT usr.usr_nombres as 'nombres', usr.usr_email as 'email', pry.pry_id as 'id_proyecto',
                    pry.pry_descripcion as 'proyecto' from usuario as usr inner join roles as rls on usr.usr_id = rls.usr_id
                    inner join proyecto as pry on rls.pry_id = pry.pry_id WHERE pry.pry_id = ? and rls.fnc_id = ? and usr.usr_estado = ? ";

            $query = $this->db->query($sql, array($pry_id, $rol, 1));

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }


    }
?>

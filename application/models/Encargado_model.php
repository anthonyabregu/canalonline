<?php

	class Encargado_model extends CI_Model{

		function __construct(){

			parent::__construct();
		}


		public function listar($estado){

            $this->db->order_by('usr_id', 'ASC');
            $query = $this->db->get_where('usuario', array('usr_estado' => $estado));

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }


		public function agregar($param){
         
          return $this->db->insert('usuario', $param);

        }

        public function editar($param, $id){
    
            $this->db->where('usr_id', $id);
            $result = $this->db->update('usuario', $param);

            return $result;
        }


        public function eliminar($usr_id){
            $this->db->where('usr_id', $usr_id);
            $result = $this->db->delete('usuario');

            return $result;
        }


        
	}
?>
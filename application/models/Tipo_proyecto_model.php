<?php

	class Tipo_proyecto_model extends CI_Model{

		function __construct(){

			parent::__construct();
		}


		public function listar($estado){

            $this->db->order_by('tip_id', 'ASC');
            $query = $this->db->get_where('tipoproyecto', array('tip_estado' => $estado));

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }


        public function agregar($param){
         
          return $this->db->insert('tipoproyecto', $param);

        }

        public function editar($param, $id){
    
            $this->db->where('tip_id', $id);
            $result = $this->db->update('tipoproyecto', $param);

            return $result;
        }


        public function eliminar($tip_id){
            $this->db->where('tip_id', $tip_id);
            $result = $this->db->delete('tipoproyecto');

            return $result;
        }
	}
?>
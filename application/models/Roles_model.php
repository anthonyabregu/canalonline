<?php

	class Roles_model extends CI_Model{

		function __construct(){

			parent::__construct();
		}


		public function listar($usr_id){

            $sql = "SELECT usr.usr_id as 'idusuario', rls.fnc_id as 'roles', usr.usr_nombres as 'nombres', 
                    usr.usr_email as 'email', pry.pry_id as 'id_proyecto', pry.pry_descripcion as 'proyecto', 
                    tip.tip_descripcion as 'tipo_proyecto' from usuario as usr inner join roles as rls on 
                    usr.usr_id = rls.usr_id inner join proyecto as pry on rls.pry_id = pry.pry_id inner join 
                    tipoproyecto tip on pry.tip_id = tip.tip_id WHERE usr.usr_id = ? ORDER BY pry.pry_id ASC";

            $query = $this->db->query($sql, array($usr_id));

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }

        public function listarPorProyecto($pry_id){

            $sql = "SELECT usr.usr_id as 'id', usr.usr_nombres as 'nombres', usr.usr_email as 'email', pry.pry_id as 'id_proyecto',
                    rls.fnc_id as 'roles', pry.pry_descripcion as 'proyecto', tip.tip_descripcion as 'tipo_proyecto' 
                    from usuario as usr inner join roles as rls on usr.usr_id = rls.usr_id inner join proyecto as 
                    pry on rls.pry_id = pry.pry_id inner join tipoproyecto tip on pry.tip_id = tip.tip_id WHERE pry.pry_id = ?
                    ORDER BY usr.usr_id ASC";

            $query = $this->db->query($sql, array($pry_id));

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }

        public function agregar($param){
         
          return $this->db->insert('roles', $param);

        }

        public function info($usr_id, $pry_id, $fnc_id){
          $query = $this->db->get_where("roles", array("usr_id" => $usr_id, "pry_id" => $pry_id, "fnc_id" => $fnc_id));
          if ($query->num_rows() == 0) return null;
          else return $query->first_row();
        }


        public function eliminar($usr_id, $pry_id, $fnc_id){

            $this->db->where('usr_id', $usr_id);
            $this->db->where('pry_id', $pry_id);
            $this->db->where('fnc_id', $fnc_id);
            $result = $this->db->delete('roles');

            return $result;
        }
	}
?>
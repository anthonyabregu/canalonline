<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Correo{
    
    protected $CI;

    public function __construct(){
    //   parent::__construct();
        $this->CI =& get_instance();
        
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'contacto@menorca.com.pe',
            'smtp_pass' => 'menorca123',
            'mailtype'  => 'html', 
            'charset'   => 'utf-8'
        );
        $this->CI->load->library('email', $config);
        $this->CI->email->initialize($config);
      
     
    }

  public function correoVenta($param){
    
     $this->CI->email->set_newline("\r\n");
    //Ponemos la dirección de correo que enviará el email y un nombre
     $this->CI->email->from('contacto@menorca.com.pe', "Menorca");

     $this->CI->email->to($param['vnt_email'], $param['vnt_nombres']);

     $this->CI->email->subject("menorca.pe - Venta Terreno");

    $row = array(
      'nombre' => $param['vnt_nombres']
    );

    $body =  $this->CI->load->view('mails/venta.php',$row,TRUE);

     $this->CI->email->message($body);
    
      if( $this->CI->email->send()){
          $envio = 'Se envió el Email.';
      }else{
          $envio = 'No se envió el Email.';
      }

      return $envio;


  }

  public function correoReferidos($param){
    // var_dump($param);
     $this->CI->email->set_newline("\r\n");
    

  //Ponemos la dirección de correo que enviará el email y un nombre
    for ($i=0; $i < count($param['nombres']) ; $i++) { 
      
         $this->CI->email->from('contacto@menorca.com.pe', "Menorca");
      
         $this->CI->email->to($param['email'][$i],$param['nombres'][$i]);

         $this->CI->email->subject('menorca.pe - Programa de referidos');

        $row = array(
          'nombre' => $param['nombres'][$i]
        );
      
        $body =  $this->CI->load->view('mails/referido.php',$row,TRUE);

         $this->CI->email->message($body);
        
        //  $this->CI->email->send();

        if( $this->CI->email->send()){
            $envio = 'Se envió el Email.';
        }else{
            $envio = 'No se envió el Email.';
        }

    }

    return $envio;


}

  public function correoContacto($param){

       $this->CI->email->set_newline("\r\n");
     
       $this->CI->email->from('contacto@menorca.com.pe', "Menorca");

       $this->CI->email->to($param['cto_email'], $param['cto_nombres']);

       $this->CI->email->subject("menorca.pe - Coordina tu visita");

      $row = array(
        'nombre' => $param['cto_nombres']
      );

      $body =  $this->CI->load->view('mails/bienvenido.php',$row,TRUE);

       $this->CI->email->message($body);
      
      //  $this->CI->email->send();

      if( $this->CI->email->send()){
          $envio = true;
      }else{
          $envio = false;
      }

      return $envio;


  }

  

}

?>